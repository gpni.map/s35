// Express set up
const express = require("express");

const mongoose = require("mongoose")


const app = express();
const port = 3001;




mongoose.connect(`mongodb+srv://gperey:admin123@zuitt-batch197.jvr3p9k.mongodb.net/S35-Activity?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})


let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection"));
db.once('open', () => console.log('Connection to MongoDB!'))


//C SCHEMA

const usersSchema = new mongoose.Schema({
	username: String,
	password: String
})



const Signup = mongoose.model('Signup', usersSchema);


//ROUTES

//Middlewares 
app.use(express.json()); // used to allow application to read JSON format data
app.use(express.urlencoded({extended:true}));

//Create a user route
app.post('/signup', (req, res) => {
	// business logic
	Signup.findOne({username: req.body.username}, (error, result) => {
		if(error){
			return res.send(error)
		} else if (result != null && result.username == req.body.username) {
			return res.send('Username exists')
		} else {
			let newUser = new Signup({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save((error, savedUser) => {
				if(error){
					return console.error(error)
				} else {
					return res.status(201).send('New User registered!')
				}
			})
		}
	})
})


app.listen(port, () => console.log(`Server is running at ${port}`));