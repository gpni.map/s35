// Express set up
const express = require("express");

//Mongoose is a package that allows creation of Schemas to model our data structures
const mongoose = require("mongoose")


const app = express();
const port = 3001;


// [Section] - Mongoose Connection
// Mongoose uses the 'connect' function to connect to the cluster in our MongoDB Atlas
// Flow

/*
	It takes 2 arguments:
	1. Connection string from our MongoDB Atlas
	2. Object that contains the middlewares/standards that MongoDB uses.
*/

mongoose.connect(`mongodb+srv://gperey:admin123@zuitt-batch197.jvr3p9k.mongodb.net/S35-Activity?retryWrites=true&w=majority`, {
	// {NewUrlParser: true} - it allows us to avoid any current and/or future error while connecting to MongoDB
	useNewUrlParser: true,
	// useUnifiedTopology is "false" by default. Set to true to opt in to using the MongoDB driver's new connection management engine
	useUnifiedTopology: true
})

// Initializes the mongoose connection to the MongoDB database by assigning mongoose.connection to the 'db' variable
let db = mongoose.connection

// Listen to the events of the connection by using the 'on()' function of the mongoose connection and it logs the details in the console based on the event (error or successful)
db.on('error', console.error.bind(console, "Connection"));

db.once('open', () => console.log('Connection to MongoDB!'))


//CREATING A SCHEMA

const taskSchema = new mongoose.Schema({
	// Define the fields with their corresponding data types
	// For a task, it needs a "task name" and its data type will be "String"
	name: String,
	// There is a field called "status" that has a data type of "String" and the default value is "Pending"
	status: {
		type: String,
		default: 'Pending'
	}
})


// MODELS
// The variable/object "Task" can now be used to run command for interacting with our DB.
// "Task" is capitalized following the MVC approach for the naming convention where we will store our data
//  The second parameter is used to specify the Schema/blueprint of the documents that will be sotred in our MongoDB collection/s
const Task = mongoose.model('Task', taskSchema);


//CREATING THE ROUTES

//Middlewares essentials in express
app.use(express.json()); // used to allow application to read JSON format data
app.use(express.urlencoded({extended:true}));

//Create a user route
app.post('/tasks', (req, res) => {
	// business logic
	Task.findOne({name: req.body.name}, (error, result) => {
		if(error){
			return res.send(error)
		} else if (result != null && result.name == req.body.name) {
			return res.send('Duplicate task found')
		} else {
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((error, savedTask) => {
				if(error){
					return console.error(error)
				} else {
					return res.status(201).send('New Task created!')
				}
			})
		}
	})
})


//Get all users from collection
app.get('/tasks', (req, res) => {
	Task.find({}, (error, result) => {
		if (error) {
			return res.send(error)
		} else {
			return res.status(200).json({
				tasks: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at ${port}`));